FROM python:3.10 as app

ENV PYTHONUNBUFFERED 1

RUN pip install poetry==1.4.2 && poetry config virtualenvs.create false

WORKDIR /application
COPY pyproject.toml .
RUN poetry install --no-dev

COPY . .

WORKDIR /application/notebook

RUN jupyter nbconvert --execute --to notebook --inplace ML_Pipeline.ipynb

WORKDIR /application/app

CMD uvicorn main:app --port 7000 --host 0.0.0.0
