import os
from os import path

import uvicorn
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, FileResponse

from config import model as trained_model
import config


app = FastAPI()


@app.post("/predict")
async def main_check(request: Request):
    data = await request.json()
    data_to_predict = list()
    data_to_predict.append(float(data.get("pregnancies", 0)))
    data_to_predict.append(float(data.get("glucose", 0)))
    data_to_predict.append(float(data.get("blood_pressure", 0)))
    data_to_predict.append(float(data.get("skin_thickness", 0)))
    data_to_predict.append(float(data.get("insulin", 0)))
    data_to_predict.append(float(data.get("bmi", 0)))
    data_to_predict.append(float(data.get("diabetes_pedigree_function", 0)))
    data_to_predict.append(float(data.get("age", 0)))

    return {"result": int(config.model.predict([data_to_predict])[0])}


@app.get("/get_model")
async def predict(model_name: str | None = None):
    if model_name is None:
        model_name = config.MODEL_NAME
    full_model_path = os.path.join(config.MODEL_DIR, model_name)
    return FileResponse(full_model_path)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=7000)
