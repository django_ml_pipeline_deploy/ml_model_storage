import os

from dotenv import load_dotenv
from joblib import load
load_dotenv()


BASE_DIR = os.path.abspath(os.path.curdir)
STATIC_DIR = os.path.abspath(os.path.join(os.path.curdir, "static"))
TEMPLATES_DIR = os.path.abspath(os.path.join(os.path.curdir, "templates"))
PARENT_DIR = os.path.abspath(os.path.join(BASE_DIR, os.pardir))
MODEL_DIR = os.path.join(PARENT_DIR, "models")

MODEL_NAME = os.getenv('MODEL_NAME', None)
MODEL_FULL_PATH = os.path.join(MODEL_DIR, MODEL_NAME)

ZERO_PREDICT_DATA = [0, 0, 0, 26, 0, 26.0, 0.647, 51]
ONE_PREDICT_DATA = [10, 160, 72, 35, 150, 33.6, 0.627, 50]

model = None


def update_model():
    global model, MODEL_FULL_PATH
    if os.path.isfile(MODEL_FULL_PATH):
        model = load(MODEL_FULL_PATH)


update_model()


if __name__ == "__main__":
    print(BASE_DIR)
    print(PARENT_DIR)
    print(MODEL_DIR)

    print(model.predict([ZERO_PREDICT_DATA]))
    print(model.predict([ONE_PREDICT_DATA]))
    # print(BASE_DIR)
    # print(BASE_DIR)
